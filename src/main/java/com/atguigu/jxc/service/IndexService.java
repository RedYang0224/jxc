package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Goods;

import java.util.List;
import java.util.Map;

public interface IndexService {

        /*获取分页*/
    List<Goods> SelectPage(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);
    /*获取总条数*/
    Integer countTotal(String codeOrName, Integer goodsTypeId);

    List<Goods> SelectTypeByPage(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    Integer counts(String goodsName, Integer goodsTypeId);
}

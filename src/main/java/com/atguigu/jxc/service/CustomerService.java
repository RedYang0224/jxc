package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> list(Integer page, Integer rows, String customerName);



    Integer count(String customerName);

    void save(Integer customerId, Customer customer);

    void delete(List<Integer> ids);

}

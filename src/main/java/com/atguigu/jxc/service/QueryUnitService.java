package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Unit;

import java.util.List;

public interface QueryUnitService {
    List<Unit> queryUnit();
}

package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerMapper;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
@Service
@SuppressWarnings("all")
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerMapper customerMapper;
    @Override
    public List<Customer> list(Integer page, Integer rows, String customerName) {
       Integer  indexPage =  (page-1)*rows;
        HashMap<String,Object> map = new HashMap<>();
        List<Customer> list = customerMapper.list(indexPage, rows, customerName);
        return list;
    }

    @Override
    public Integer count(String customerName) {
       Integer count = customerMapper.cout(customerName);
        return count;
    }

    @Override
    public void save(Integer customerId, Customer customer) {
        if (customerId == null){
            customerMapper.save(customer);
        }else {
            customerMapper.update(customerId,customer);

        }
    }

    /**
     * 批量删除数据
     * @param ids
     */
    @Override
    public void delete(List<Integer> ids) {
            customerMapper.delete(ids);
    }

}

package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.QueryUnitMapper;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.QueryUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
@Service
@SuppressWarnings("all")
public class QueryUnitServiceImpl implements QueryUnitService {
    @Autowired
    private QueryUnitMapper queryUnitMapper;
    //查询商品的单位
    @Override
    public List<Unit> queryUnit() {
        List<Unit>  unitList = queryUnitMapper.queryUnit();

        return unitList;
    }
}

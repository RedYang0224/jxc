package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.jxc.dao.IndexDao;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.IndexService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
@Service
public class IndexServiceImpl implements IndexService {
    /**
     * 分页查询首页数据
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */

    @Resource
    private IndexDao indexDao;


    @Override
    public List<Goods> SelectPage(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        //分页的的公式
        Integer startIndex = (page-1)*rows;

        List <Goods>  goodsList =   indexDao.selectPage(startIndex,rows,codeOrName,goodsTypeId);

        return goodsList;

    }

    @Override
    public Integer countTotal(String codeOrName, Integer goodsTypeId) {
        Integer integer = indexDao.sleectTotal(codeOrName, goodsTypeId);
        return integer;
    }

    @Override
    public List<Goods> SelectTypeByPage(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {

        List <Goods>  goodsList =   indexDao.selectPageByType(page,rows,goodsName,goodsTypeId);
        return goodsList;
    }

    @Override
    public Integer counts(String goodsName, Integer goodsTypeId) {
        Integer integer = indexDao.count(goodsName, goodsTypeId);


        return integer;
    }


}

package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierMapper;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
@Service

public class SupplierServiceImpl implements SupplierService {
    @Resource
    private SupplierMapper supplierMapper;


    @Override
    public List<Supplier> selectSupplierList(Integer page, Integer rows, String supplierName) {
//           分页：（当前页数-1）*总行数
          Integer startPage = (page-1)*rows;
        List<Supplier> supplierList = supplierMapper.selectSupplierList(startPage, rows, supplierName);



        return supplierList;
    }

    @Override
    public Integer selectSupplierListCount(String supplierName) {
    Integer totalCount = supplierMapper.selectSupplierListCount(supplierName);


        return totalCount;
    }

    @Override
    public ServiceVO saveSupplierOrUpdate(Integer supplierId, Supplier supplier) {

        if (null == supplierId){
            supplierMapper.saveSupplier(supplier);
            return new ServiceVO(100, "请求成功", null);
        }else {
          supplierMapper.updateSupplier(supplierId,supplier);
            return new ServiceVO(100, "请求成功", null);

        }


    }

    @Override
    public ServiceVO deleteSupplier(List<Integer> idList) {

       supplierMapper.deleteSupplie(idList);
        return new ServiceVO(100,"请求成功", null);
    }


}

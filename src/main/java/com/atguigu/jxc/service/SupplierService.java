package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.List;

public interface SupplierService {
    List<Supplier> selectSupplierList(Integer page, Integer rows, String supplierName);

    Object selectSupplierListCount(String supplierName);


    ServiceVO saveSupplierOrUpdate(Integer supplierId, Supplier supplier);


    ServiceVO deleteSupplier(List<Integer> idList);
}

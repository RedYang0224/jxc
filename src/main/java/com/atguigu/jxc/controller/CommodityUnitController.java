package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.QueryUnitService;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.management.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Api(tags = "商品管理")
@RestController
@SuppressWarnings("all")
@RequestMapping("unit")
public class CommodityUnitController {
    @Autowired
    private QueryUnitService queryUnitService;


    @PostMapping("list")
    public Map<String,Object> queryUnit(){
        HashMap<String, Object> map = new HashMap<>();


        List<Unit> unitList =  queryUnitService.queryUnit();


        map.put("rows",unitList);
        return map;





    }

}

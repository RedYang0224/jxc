package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Api(tags = "客户管理")
@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @ApiOperation("保存或更新客户数据")
    @PostMapping("/save")

    public ServiceVO  save(@RequestParam(value = "customerId",required = false) Integer customerId ,Customer customer){
            customerService.save(customerId,customer);
            return  new ServiceVO(100,"保存成功",null);
    }

    @PostMapping("/list")
    @ApiOperation("分页查询客户数据")
    @ResponseBody
    public Map<String, Object> list(@RequestParam("page") Integer page, @RequestParam("rows") Integer rows, @RequestParam(value = "customerName",required = false) String customerName) {
        HashMap<String, Object> map = new HashMap<>();
        //查询分页
        List<Customer> list = customerService.list(page, rows, customerName);
        Integer countPage = customerService.count(customerName);
        map.put("total", countPage);
        map.put("rows", list);
        return map;

    }

    @PostMapping("/delete")
    @ApiOperation("删除批量客户数据")
    public ServiceVO delete(@RequestParam("ids") String ids){
//        ArrayList<Integer> list = new ArrayList<>();
//        String[] split = ids.split(",");
//        for (String s : split) {
//            list.add(Integer.parseInt(s));
//        }
        //流处理
        List<Integer> idList = Arrays.stream(ids.split(","))
                .map(Integer::parseInt).collect(Collectors.toList());



        customerService.delete(idList);


        return  new ServiceVO(100,"删除成功",null);


    }



}

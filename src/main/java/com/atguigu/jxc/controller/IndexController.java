package com.atguigu.jxc.controller;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.service.IndexService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 系统首页请求控制器
 */

@Controller
public class IndexController {

    @Autowired
    private IndexService indexService;


    /**
     * 进入登录页面
     *
     * @return 重定向至登录页面
     */
    @GetMapping("/")
    public String toIndex() {
        return "redirect:login.html";
    }

    //	http://localhost:9999/goods/listInventory
    //显示后台首页页面显示

    @ApiOperation(value = "获取首页详细信息")
    @ResponseBody
    @PostMapping("/goods/listInventory")
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        //构建存储rows的集合
        HashMap<String, Object> map = new HashMap<>();
        List<Goods> goodsList = indexService.SelectPage(page, rows, codeOrName, goodsTypeId);
        Integer total = indexService.countTotal(codeOrName, goodsTypeId);
        map.put("total", total);
        map.put("rows", goodsList);
        return map;
    }

    @ApiOperation("根据类型分类分页")
    @ResponseBody
    @PostMapping("/goods/list")
    public Map<String,Object> listType(Integer page,
                                       Integer rows,
                                       String goodsName,
                                      Integer goodsTypeId){
        HashMap<String, Object> map = new HashMap<>();


        List<Goods> listType =  indexService.SelectTypeByPage(page, rows, goodsName, goodsTypeId);
//        Integer total = indexService.counts(goodsName, goodsTypeId);

        map.put("total", listType.size());
        map.put("rows",listType);
        System.out.println("Test1hhhhh分支");


        return map;


    }




}

package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Delete;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@Api(tags = "供应商")
@RequestMapping("/supplier")
public class SupplierController {
    @Resource
    private SupplierService supplierService;


    //        查询分页
    @ApiOperation("分页查询供应商")
    @PostMapping("/list")
    @ResponseBody
    public Map<String, Object> supplierList(@RequestParam(value = "page") Integer page,
                                            @RequestParam(value = "rows") Integer rows,
                                            @RequestParam(required = false) String supplierName) {
        HashMap<String, Object> map = new HashMap<>();
        List<Supplier> supplierList = supplierService.selectSupplierList(page, rows, supplierName);
        map.put("rows", supplierList);
        map.put("total", supplierService.selectSupplierListCount(supplierName));

        return map;


    }
@ApiOperation("添加或修改供应商")
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO saveSupplier( Integer supplierId, Supplier supplier) {
            return supplierService.saveSupplierOrUpdate(supplierId,supplier);
    }

    @ApiOperation("批量删除供应商")
    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO deleteSupplier(String ids){
    List<Integer> idList = Arrays.stream(ids.split(","))
            .map(Integer::parseInt).collect(Collectors.toList());
        return supplierService.deleteSupplier(idList);

    

    }

}

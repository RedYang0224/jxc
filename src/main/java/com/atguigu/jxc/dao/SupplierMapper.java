package com.atguigu.jxc.dao;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierMapper {


     void deleteSupplie(List<Integer> idList) ;

    List<Supplier> selectSupplierList(@Param("startPage") Integer page, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

     Integer selectSupplierListCount(@Param("supplierName") String supplierName);


      void saveSupplier(Supplier supplier);

      void updateSupplier(@Param("supplierId") Integer supplierId,
                          @Param("supplier") Supplier supplier);
}

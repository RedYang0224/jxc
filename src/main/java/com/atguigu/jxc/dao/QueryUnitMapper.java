package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface QueryUnitMapper {


    List<Unit> queryUnit();

}

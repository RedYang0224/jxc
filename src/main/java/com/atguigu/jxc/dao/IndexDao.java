package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Map;

public interface IndexDao {
    List<Goods> selectPage(@Param("startIndex") Integer startIndex, @Param("rows") Integer rows, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    Integer sleectTotal(@Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);


    List<Goods> selectPageByType(@Param("startIndex") Integer startIndex, @Param("rows") Integer rows, @Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    Integer count(@Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);
}

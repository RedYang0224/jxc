package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerMapper {




    List<Customer> list(@Param("indexPage") Integer indexPage, @Param("rows") Integer rows, @Param("customerName") String customerName);

    Integer cout(@Param("customerName") String customerName);

    void save(@Param("customer") Customer customer);


    void update(@Param("customerId") Integer customerId, @Param("customer") Customer customer);


    void delete(@Param("ids") List<Integer> ids);

}
